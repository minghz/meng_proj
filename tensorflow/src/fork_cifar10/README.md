# CIFAR-10 on LeNet with Fixpoint precision conversion #

This is a modification upon the CIFAR-10 tutorial provided by Tensorflow.
The original code/tutorial can be found here:

> CIFAR-10 is a common benchmark in machine learning for image recognition.

> http://www.cs.toronto.edu/~kriz/cifar.html

> Code in this directory demonstrates how to use TensorFlow to train and evaluate a convolutional neural network (CNN) on both CPU and GPU. We also demonstrate how to train a CNN over multiple GPUs.

> Detailed instructions on how to get started available at:

> http://tensorflow.org/tutorials/deep_cnn/

This particular fork contains the following main additions:

* Custom Operation in C++ inside `custom_ops/` which performs fixed point accuracy conversion.
* Method `update_fix_point_accuracy` that updates bit width for fraction or digit bits according to a tolerance threshold.
* `conv_layer` and `nn_layer` methods that creates convolution layer, fully connected layer and perform fixed point operations
* `_to_fixed_point` method that converts to fixed point accuracy
* `_fixed_point_conversion_summary` method that summarizes tensor before and after fixed point conversion

and the following main changes:

* From `MonitoredSession` to `InteractiveSession`
* `FLAGS` default values changed from storing to `tmp/` to `./`

along with now potentially broken features (untested, with no guarantees):

* Any distributed support (CPU or GPU)