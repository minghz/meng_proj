# Final copy. this one should be free of bugs #
* Has a simple gradient function (pass-through) 
* Fixed the memory leak. It happened because extra operands were being constantly added to the graph
* Has fixed point accuracy settings for Weights as well as Biases
    * This actually is dynamic for ALL layers (weights, biases, activations)

# Next Steps #
* Certify that my gradient implementation is correct
* Devise a plan to adjust fix point precision in chunks of steps
* How does other techniques influence calcuations? (i.e. Adam optimizer vs, Gradient Descent, or Dropout)
* How do other NN models behave? (i.e. distribution of weights, biases, activations may differ, requierinig different fixed point number width/definitions)
