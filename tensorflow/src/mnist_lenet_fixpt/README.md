# Description #
This tensorflow project describes a model with the following configurations:

* MNIST dataset input - train network to recognize hand-written digits 0-9
* LeNet network model - hidden layers are two convolution layers (conv, pool), one fully connected layer.
* Dropout technique to reduce overfitting
* Fixed point conversion simulation. Convert float32 into a value-equivalent fixed-point number. This is done via a custom operation in C++

# How to run #
> python run.py

# Backup summaries
Basically its just moving it to `past_summaries` dir and zipping that dir

    > tar -xzvf past_summaries.tar.gz
    > mv summaries.<the name of the summary> ./past_summaries
    > tar -czvf past_summaries.tar.gz ./past_summaries
