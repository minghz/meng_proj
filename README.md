# M.Eng. Project
Research with Tensorflow and Fixed point accuracy for training

## Environemnt Setup
Using Virtual Env to keep all python version the same.
Code for network models found in `tensorflow/src`

* Tensorflow version: 1.4
* Python version 3.6.3

## For More Documentation on included networks
Please check `README.md` from the individual directories within `tensorflow/src`
